package anvil.loader;

import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;

import java.util.ArrayList;
import java.util.List;

public class Visitor extends ClassVisitor {

    public List<String> annotatiions = new ArrayList<>();

    public Visitor() {
        super(Opcodes.ASM5);
    }

    @Override
    public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
        annotatiions.add(desc.replace('/', '.').replaceAll(";", "").substring(1));
        return null;
    }
}
