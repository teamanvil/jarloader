package anvil.loader;

import org.objectweb.asm.ClassReader;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

public class Scanner {

    public List<ClassAnnotations> scanForAnnotations(String[] annotations) {
        List<ClassAnnotations> classAnnotations = new ArrayList<>();

        for (URL url: getRootUrls ()) {
            File f = new File (url.getPath());
            if (!f.isDirectory()){
                try {
                    classAnnotations.addAll(visitJar(url));
                } catch (IOException e) {

                }
            }
        }

        List<ClassAnnotations> toRemove = new ArrayList<>();
            for (ClassAnnotations ca : classAnnotations) {
                removeotherAnnotations(ca, annotations);
                if (ca.annotation.size() == 0) {

                    toRemove.add(ca);
                }
            }

            for (ClassAnnotations ca : toRemove) {
                classAnnotations.remove(ca);
            }

        return classAnnotations;

    }

    List<ClassAnnotations> visitJar (URL url) throws IOException {
        List<ClassAnnotations> classAnnotations = new ArrayList<>();
        try (InputStream urlIn = url.openStream ();
             JarInputStream jarIn = new JarInputStream (urlIn)) {
            JarEntry entry;
            while ((entry = jarIn.getNextJarEntry ()) != null) {
                if (entry.getName ().endsWith (".class")) {
                    String name = entry.getName();
                    name = name.replaceAll("/", "\\.");
                    name = name.replaceAll("\\.class", "");

                    ClassAnnotations ca = new ClassAnnotations(name);
                    ca.annotation = getClassAnnotations(name);
                    if (ca.annotation.size() > 0) {
                        classAnnotations.add(ca);
                    }
                }
            }
        }
        return classAnnotations;
    }

    public List<String> getClassAnnotations(String className) {
        Visitor classVisitor = new Visitor();

        try {
            new ClassReader(className).accept(classVisitor, 0);
            return classVisitor.annotatiions;
        } catch (IOException e) {
            System.out.println(e.getLocalizedMessage() + ": " + className);
            return new ArrayList<>();

        }


    }

    public static class ClassAnnotations {

        public String className;
        public List<String> annotation;

        public ClassAnnotations(String className) {
            this.className = className;
            annotation = new ArrayList<>();
        }

    }

    public List<URL> getRootUrls () {
        List<URL> result = new ArrayList<> ();

        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        while (cl != null) {
            if (cl instanceof URLClassLoader) {
                URL[] urls = ((URLClassLoader) cl).getURLs();
                result.addAll (Arrays.asList (urls));
            }
            cl = cl.getParent();
        }
        return result;
    }

    void removeotherAnnotations(ClassAnnotations classAnnotations, String[] toKeep) {

        List<String> toRemove = new ArrayList<>();
        for (String annotation : classAnnotations.annotation) {
            boolean keep = false;
            for (String an : toKeep) {
                if (annotation.equals(an)) {
                    keep = true;
                }
            }

            if (!keep) {
                toRemove.add(annotation);
            }
        }

        for (String s : toRemove) {
            classAnnotations.annotation.remove(s);
        }

    }

}
