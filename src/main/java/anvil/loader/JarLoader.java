package anvil.loader;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.JarFile;

public class JarLoader {


    public File[] getJarsInFolder(File directory) {

        File[] files = directory.listFiles();
        List<File> jars = new ArrayList<>();

        for (File file : files) {
            String[] fileName = file.getName().split("\\.");
            if (fileName[fileName.length - 1].equals("jar")) {
                jars.add(file);

            }

        }

        return jars.toArray(new File[0]);
    }

    public void addToClasspath(File jar) {

        try {

            URL url = jar.toURI().toURL();

            URLClassLoader classLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
            Method load = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
            load.setAccessible(true);
            load.invoke(classLoader, url);

        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }

    }

    public JarFile tojar(File jar) {
        try {
            return new JarFile(jar);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
